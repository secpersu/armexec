/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _EXEC_H
#define _EXEC_H

#include <stdint.h>

struct armld;

typedef struct {
    char        *name;
    uint32_t    entry;
    uint32_t    lr;
} exec_frame_t;

int
arm_exec(struct armld *ld,
         uint32_t start,
         uint32_t halt,
         uint32_t *args,
         int cnt);

void
exec_load_registers(uint32_t core[16], int cpsr[4]);

exec_frame_t *
exec_get_backtrace(int *depth);

void
exec_set_r1(uint32_t r);

void
exec_set_breakpoint(uint32_t addr);

#endif

/*
 * This file is part of armexec.
 *
 * armexec is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * armexec is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with armexec.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <errno.h>
#include <limits.h>

#include "armexec.h"
#include "elf.h"

/* from plt_stub.c */
extern int          g_plt_stubs_cnt;
extern elf_stub_t   g_plt_stubs[];
extern int          g_prog_stubs_cnt;
extern elf_stub_t   g_prog_stubs[];

/* from libc_obj.c */
extern elf_obj_t    g_libc_objs[];
extern int          g_libc_obj_cnt;

typedef struct {
    Elf32_Shdr      *sh;
    char            *name;
    char            *raw;
    uint32_t        start;
    uint32_t        end;
    int             in_memory;
} elf_sh_t;

typedef struct {
    uint32_t        addr;
    uint32_t        size;
    char            *name;
} elf_sym_t;

struct elf {
    struct vm       *vm;
    int             sh_cnt;
    elf_sh_t        *secs;
    char            *strtab;

    /* These are in memory */
    elf_sh_t        *text;
    elf_sh_t        *rodata;
    elf_sh_t        *got;
    elf_sh_t        *data;
    elf_sh_t        *bss;
    elf_sh_t        *plt;

    /* These are not in memory */
    elf_sh_t        *rel_plt;
    elf_sh_t        *rel_dyn;
    elf_sh_t        *dynsym;
    elf_sh_t        *dynstr;

    elf_stub_t      *plt_stubs;
    int             plt_cnt;

    elf_stub_t      *sub_stubs;
    int             stub_cnt;

    elf_sym_t       *symbols;
    int             sym_cnt;
};

static elf_sh_t *
elf_get_section(struct elf *elf, char *name);

static char *
elf_lookup_section(struct elf *elf, uint32_t addr);

static elf_sym_t *
elf_lookup_symbol(struct elf *elf, char *name);

static const char *required_sections[] = {
    ".dynsym", ".dynstr", ".rel.plt", ".plt",
    ".text", ".rodata", ".got", ".data", ".bss",
    ".rel.dyn", ".data.rel.ro", ".data.rel.ro.local"
};

static const int SECTION_CNT = sizeof(required_sections) / sizeof(char *);

static int
is_required_section(char *name)
{
    int i;

    for (i = 0; i < SECTION_CNT; i++)
        if (strcmp(name, required_sections[i]) == 0)
            return 1;

    return 0;
}

static elf_sh_t *
elf_get_section(struct elf *elf, char *name)
{
    int i;
    elf_sh_t *sh;

    sh = elf->secs;
    for (i = 0; i < elf->sh_cnt; i++, sh++) {
        if (strcmp(name, sh->name) == 0)
            return sh;
    }

    return NULL;
}

void *
elf_phy_addr(struct elf *elf, uint32_t addr)
{
    int i;
    elf_sh_t *sh;
    int cnt;

    if (addr == VM_INVALID_ADDR || !addr)
        return NULL;

    sh = elf->secs;
    for (i = 0; i < elf->sh_cnt; i++, sh++) {
        if (!sh->in_memory)
            continue;
        if (addr >= sh->start && addr < sh->end)
            return sh->raw + (addr - sh->start);
    }

    return NULL;
}

void
elf_add_stubs(struct elf *elf, elf_stub_t *stubs, int cnt)
{
    int prev = elf->stub_cnt;
    int i, j;

    /* resolve addr of stub */
    for (i = 0; i < cnt; i++) {
        /* usr knows the addr */
        if (stubs[i].addr)
            continue;

        for (j = 0; j < elf->sym_cnt; j++) {
            if (strcmp(elf->symbols[j].name, stubs[i].name) == 0) {
                stubs[i].addr = elf->symbols[j].addr;
                DEBUG("Add stub [%s]:%08x\n", stubs[i].name, stubs[i].addr);
                break;
            }
        }
    }

    elf->stub_cnt += cnt;
    elf->sub_stubs = realloc(elf->sub_stubs,
                             elf->stub_cnt * sizeof(elf_stub_t));
    memcpy(&elf->sub_stubs[prev], stubs, cnt * sizeof(elf_stub_t));
}

elf_stub_t *
elf_lookup_stub(struct elf *elf, uint32_t addr)
{
    int i;
    elf_stub_t *stub;

    stub = elf->plt_stubs;

    if (addr >= elf->plt->start && addr < elf->plt->end) {
        for (i = 0; i < elf->plt_cnt; i++, stub++) {
            if (addr == stub->addr) {
                if (stub->func)
                    return stub;
                break;
            }
        }
        /* A plt must have a stub */
        PANIC("plt stub missing: [%s]:%08x\n",
              stub->name, addr);

        return NULL;
    }

    for (i = 0; i < elf->stub_cnt; i++) {
        if (addr == elf->sub_stubs[i].addr)
            return &elf->sub_stubs[i];
    }

    return NULL;
}

static void
resolve_symbols(struct elf *elf)
{
    Elf32_Sym *esym;
    elf_sym_t *sym;
    int i, cnt;

    esym = (Elf32_Sym *) elf->dynsym->raw;
    esym++;
    cnt = elf->dynsym->sh->sh_size / sizeof(Elf32_Sym);
    cnt--;
    sym = calloc(cnt, sizeof(elf_sym_t));
    elf->sym_cnt = cnt;
    elf->symbols = sym;

    for (i = 1; i <= cnt; i++, sym++, esym++) {
        sym->size = esym->st_size;
        sym->addr = esym->st_value;
        sym->name = &elf->strtab[esym->st_name];
        if (ELF32_ST_TYPE(esym->st_info) == STT_FUNC)
            sym->addr &= ~1;
    }
}

static void
resolve_plt(struct elf *elf)
{
    Elf32_Rel *rel;
    Elf32_Sym *sym;
    int i, j, cnt;
    uint32_t plt_ent;
    elf_stub_t *stub;

    sym = (Elf32_Sym *) elf->dynsym->raw;
    rel = (Elf32_Rel *) elf->rel_plt->raw;
    plt_ent = elf->plt->start;
    /* skip the PLT0 */
    plt_ent += 0x14;

    cnt = elf->rel_plt->sh->sh_size / sizeof(Elf32_Rel);

    elf->plt_stubs = calloc(cnt, sizeof(elf_stub_t));
    elf->plt_cnt = cnt;
    stub = elf->plt_stubs;

    for (i = 0; i < cnt; i++, rel++, stub++) {
        j = rel->r_info >> 8;
        stub->name = &elf->strtab[sym[j].st_name];
        stub->addr = plt_ent;
        /* 0xc is sizeof each PLT entry */
        plt_ent += 0xc;
    }
}

static void
resolve_rel_dyn(struct elf *elf)
{
    Elf32_Rel *rel;
    Elf32_Sym *sym;
    int i, j, k, cnt;
    char *name;
    struct vm *vm = elf->vm;
    elf_stub_t *plt_stub = elf->plt_stubs;
    elf_obj_t *obj;

    sym = (Elf32_Sym *) elf->dynsym->raw;
    rel = (Elf32_Rel *) elf->rel_dyn->raw;

    cnt = elf->rel_dyn->sh->sh_size / sizeof(Elf32_Rel);

    for (i = 0; i < cnt; i++, rel++) {
        int found = 0;

        j = rel->r_info >> 8;
        name = &elf->strtab[sym[j].st_name];
        if (!name[0])
            continue;

        /* It could be external obj */
        /* Cannot use SW here, coz vm->elf has not been set yet */
        obj = g_libc_objs;
        for (k = 0; k < g_libc_obj_cnt; k++, obj++) {
            if (strcmp(obj->name, name) == 0) {
                uint32_t data, pdata;
                void *p;

                found = 1;
                data = vm_malloc(vm, obj->size, &p);
                memcpy(p, obj->data, obj->size);

                if (obj->type == ELF_OBJ_T_DATA) {
                    *(uint32_t *)elf_phy_addr(elf, rel->r_offset) = data;
                } else {
                    pdata = vm_malloc(vm, 4, NULL);
                    SW(pdata, data);
                    *(uint32_t *)elf_phy_addr(elf, rel->r_offset) = pdata;
                }
            }
        }

        if (found)
            continue;

        /* or could be in plt */
        for (k = 0; k < elf->plt_cnt; k++) {
            if (strcmp(plt_stub[k].name, name) == 0) {
                *(uint32_t *)elf_phy_addr(elf, rel->r_offset) = plt_stub[k].addr;
                break;
            }
        }
    }
}

static void
load_plt_stubs(struct elf *elf)
{
    int i, j;
    elf_stub_t *stub;

    stub = elf->plt_stubs;
    for (i = 0; i < elf->plt_cnt; i++, stub++) {
        for (j = 0; j < g_plt_stubs_cnt; j++) {
            if (strcmp(g_plt_stubs[j].name, stub->name) == 0) {
                DEBUG("Load stub [%s]:%08x\n", stub->name, stub->addr);
                stub->func = g_plt_stubs[j].func;
                break;
            }
        }
    }
}

static elf_sym_t *
elf_lookup_symbol(struct elf *elf, char *name)
{
    int i;
    elf_sym_t *s;

    s = elf->symbols;
    for (i = 0; i < elf->sym_cnt; i++, s++) {
        if (strcmp(s->name, name) == 0)
            return s;
    }

    return NULL;
}

char *
elf_lookup_name(struct elf *elf, uint32_t addr)
{
    int i;
    elf_sym_t *s;
    elf_stub_t *stub;

    s = elf->symbols;
    for (i = 0; i < elf->sym_cnt; i++, s++) {
        if (s->addr == addr)
            return s->name;
    }

    stub = elf->plt_stubs;
    for (i = 0; i < elf->plt_cnt; i++, stub++) {
        if (stub->addr == addr)
            return stub->name;
    }

    return NULL;
}

static char *
elf_lookup_section(struct elf *elf, uint32_t addr)
{
    int i;
    elf_sh_t *sh;

    sh = elf->secs;
    for (i = 0; i < elf->sh_cnt; i++, sh++) {
        if (!sh->in_memory)
            continue;
        if (addr >= sh->start && addr < sh->end)
            return sh->name;
    }

    return NULL;
}

struct elf *
elf_load(struct vm *vm, FILE *fp)
{
    Elf32_Ehdr ehdr;
    Elf32_Shdr *shdrs, *shstr, *psh;
    char *shstab;
    struct elf *elf;
    elf_sh_t *esh;
    int i, ret;

    fseek(fp, 0, SEEK_SET);
    /* read file header */
    ret = fread(&ehdr, sizeof(ehdr), 1, fp);
    if (ret != 1)
        goto out;

    /* read all section headers */
    fseek(fp, ehdr.e_shoff, SEEK_SET);
    shdrs = calloc(ehdr.e_shnum, ehdr.e_shentsize);
    ret = fread(shdrs, ehdr.e_shentsize, ehdr.e_shnum, fp);
    if (ret != ehdr.e_shnum)
        goto out;

    /* read section name string table */
    shstr = &shdrs[ehdr.e_shstrndx];
    shstab = calloc(1, shstr->sh_size);
    fseek(fp, shstr->sh_offset, SEEK_SET);
    ret = fread(shstab, shstr->sh_size, 1, fp);
    if (ret != 1)
        goto out;

    /* read all required sections */
    esh = calloc(SECTION_CNT, sizeof(*esh));
    elf = calloc(1, sizeof(*elf));

    elf->vm = vm;
    elf->secs = esh;
    elf->sh_cnt = SECTION_CNT;
    /* First section header is null */
    psh = shdrs + 1;

    for (i = 1; i < ehdr.e_shnum; i++, psh++) {
        char *sec;

        if (!is_required_section(&shstab[psh->sh_name]))
            continue;

        esh->sh = psh;
        esh->name = &shstab[psh->sh_name];
        esh->start = psh->sh_addr;
        esh->end = psh->sh_addr + psh->sh_size;

        if (psh->sh_type == SHT_PROGBITS)
            esh->in_memory = 1;

        /* .bss has no place in file, but the section hdr is needed */
        if (psh->sh_type != SHT_NOBITS) {
            sec = calloc(1, psh->sh_size);
            fseek(fp, psh->sh_offset, SEEK_SET);
            ret = fread(sec, psh->sh_size, 1, fp);
            if (ret != 1)
                goto out;
            esh->raw = sec;

            if (strcmp(esh->name, ".dynstr") == 0)
                elf->strtab = sec;
        }

        esh++;
    }

    /* assign all sections */
    elf->text = elf_get_section(elf, ".text");
    elf->dynsym = elf_get_section(elf, ".dynsym");
    elf->dynstr = elf_get_section(elf, ".dynstr");
    elf->got = elf_get_section(elf, ".got");
    elf->dynsym = elf_get_section(elf, ".dynsym");
    elf->rodata = elf_get_section(elf, ".rodata");
    elf->data = elf_get_section(elf, ".data");
    elf->plt = elf_get_section(elf, ".plt");

    elf->rel_plt = elf_get_section(elf, ".rel.plt");
    elf->rel_dyn = elf_get_section(elf, ".rel.dyn");

    elf->bss = elf_get_section(elf, ".bss");
    elf->bss->in_memory = 1;
    elf->bss->raw = calloc(1, elf->bss->sh->sh_size);

    /* resolve all symbols */
    resolve_symbols(elf);

    /* calculate all plt entries */
    resolve_plt(elf);
    load_plt_stubs(elf);

    /* inject extern objects */
    resolve_rel_dyn(elf);

    /* add prog stubs */
    elf_add_stubs(elf, g_prog_stubs, g_prog_stubs_cnt);

    return elf;

out:
    perror("fread");
    return NULL;
}
